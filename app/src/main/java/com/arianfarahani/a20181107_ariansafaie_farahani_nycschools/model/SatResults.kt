package com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SatResults(

    @field:Json(name = "dbn")
    val dbn: String,

    @field:Json(name = "sat_critical_reading_avg_score")
    val reading: String,

    @field:Json(name = "sat_math_avg_score")
    val math: String,

    @field:Json(name = "sat_writing_avg_score")
    val writing: String

) : Parcelable