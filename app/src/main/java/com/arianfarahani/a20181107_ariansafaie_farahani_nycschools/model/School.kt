package com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class School(

    @field:Json(name = "school_name")
    val name: String,

    @field:Json(name = "website")
    val website: String,

    @field:Json(name = "overview_paragraph")
    val overview: String,

    @field:Json(name = "dbn")
    val dbn: String

) : Parcelable