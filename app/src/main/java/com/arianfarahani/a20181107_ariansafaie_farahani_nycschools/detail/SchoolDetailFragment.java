package com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.R;
import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model.SatResults;
import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model.School;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class SchoolDetailFragment extends Fragment {

    private static final String ARG_SCHOOL = "school.detail";
    private static final String ARG_SAT = "sat.detail";

    public static SchoolDetailFragment newInstance(
            @NonNull School school,
            @NonNull SatResults results
    ) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_SCHOOL, school);
        bundle.putParcelable(ARG_SAT, results);
        SchoolDetailFragment fragment = new SchoolDetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_school_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*
         * Definitely a bit of a plain look here. Of course in a full app this would be fully designed
         * with a lot more useful information put in :)
         */
        School school = getArguments().getParcelable(ARG_SCHOOL);
        SatResults results = getArguments().getParcelable(ARG_SAT);

        TextView name = view.findViewById(R.id.school_name);
        setTextWithVisbility(name, school.getName());

        TextView website = view.findViewById(R.id.school_website);
        setTextWithVisbility(website, school.getWebsite());

        TextView description = view.findViewById(R.id.school_overview);
        setTextWithVisbility(description, school.getOverview());

        if(results == null) {
            view.findViewById(R.id.results_container).setVisibility(View.GONE);
            view.findViewById(R.id.results_label).setVisibility(View.GONE);
        } else {
            TextView mathScore = view.findViewById(R.id.text_math_score);
            setTextWithVisbility(mathScore, results.getMath());

            TextView readingScore = view.findViewById(R.id.text_reading_score);
            setTextWithVisbility(readingScore, results.getReading());

            TextView writingScore = view.findViewById(R.id.text_writing_score);
            setTextWithVisbility(writingScore, results.getWriting());
        }
    }

    /**
     * Handles hiding view if data isn't parsed from the JSON
     * This makes for a nice extension function in kotlin
     */
    private void setTextWithVisbility(TextView textView, String text) {
        if (textView == null)
            return;

        if (text == null || text.isEmpty()) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(text);
        }
    }
}
