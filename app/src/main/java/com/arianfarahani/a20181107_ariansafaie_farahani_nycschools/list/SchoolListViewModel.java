package com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.list;

import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.api.SchoolRepository;
import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model.SatResults;
import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model.School;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SchoolListViewModel extends ViewModel {

    private MutableLiveData<List<School>> schoolLiveData = new MutableLiveData<>();
    private List<SatResults> satResults;

    public LiveData<List<School>> getSchools() {
        return schoolLiveData;
    }

    public void updateSchoolData() {
        SchoolRepository.fetchSchools((success, results) -> {
            if(success && results != null) {
                schoolLiveData.setValue(results);
            } else {
                // could show a nice error message here (maybe lost internet connection)
            }
            return null;
        });

        if(satResults == null)
            updateSatResults(); // could implement a staleness property
    }

    public SatResults getMatchingSatResults(School school) {
        for(SatResults result : satResults) {
            if(result.getDbn().equals(school.getDbn()))
                return result;
        }

        return null; // no matching result found
    }

    private void updateSatResults() {
        SchoolRepository.fetchSatResults((success, results) -> {
            if(success && results != null) {
                satResults = results;
            } else {
                // could show a nice error message here (maybe lost internet connection)
            }
            return null;
        });
    }
}
