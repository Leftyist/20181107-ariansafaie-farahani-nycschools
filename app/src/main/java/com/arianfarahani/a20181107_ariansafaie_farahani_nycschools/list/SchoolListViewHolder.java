package com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.list;

import android.view.View;
import android.widget.TextView;

import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.R;
import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model.School;

import androidx.recyclerview.widget.RecyclerView;

class SchoolListViewHolder extends RecyclerView.ViewHolder {

    private final TextView nameText;
    private final SchoolListAdapter.SchoolSelectedListener listener;

    SchoolListViewHolder(View view, SchoolListAdapter.SchoolSelectedListener listener) {
        super(view);
        nameText = (TextView) view.findViewById(R.id.school_name);
        this.listener = listener;
    }

    void bind(School school) {
        nameText.setText(school.getName());
        itemView.setOnClickListener( v -> {
            listener.onSelect(school);
        });
    }
}
