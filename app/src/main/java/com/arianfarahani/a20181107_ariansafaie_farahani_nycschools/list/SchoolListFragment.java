package com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.MainActivity;
import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.R;
import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model.SatResults;
import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model.School;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SchoolListFragment extends Fragment implements
        SchoolListAdapter.SchoolSelectedListener {

    private SchoolListViewModel viewModel;

    public static SchoolListFragment newInstance() {
        return new SchoolListFragment();
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragment_school_list, container, false);
    }

    @Override
    public void onViewCreated(
            @NonNull View view, @Nullable Bundle savedInstanceState
    ) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = ViewModelProviders.of(this).get(SchoolListViewModel.class);

        viewModel.getSchools().observe(this, schools -> {
            if (schools != null && schools.size() > 0) {
                RecyclerView recyclerView = view.findViewById(R.id.item_list);
                SchoolListAdapter adapter = new SchoolListAdapter(schools, this);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                recyclerView.setAdapter(adapter);
            }
        });

        viewModel.updateSchoolData();
    }

    @Override
    public void onSelect(School school) {
        SatResults results = viewModel.getMatchingSatResults(school);
        ((MainActivity) getActivity()).showDetails(school, results);
    }
}
