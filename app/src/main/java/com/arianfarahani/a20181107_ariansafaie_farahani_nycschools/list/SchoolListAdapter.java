package com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.R;
import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model.School;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SchoolListAdapter extends RecyclerView.Adapter<SchoolListViewHolder> {

    private List<School> schoolList;
    private SchoolSelectedListener listener;

    SchoolListAdapter(List<School> schools, SchoolSelectedListener listener) {
        this.schoolList = schools;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SchoolListViewHolder onCreateViewHolder(
            @NonNull ViewGroup parent, int viewType
    ) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_school_item, parent, false);
        return new SchoolListViewHolder(v, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolListViewHolder holder, int position) {
        holder.bind(schoolList.get(position));
    }

    @Override
    public int getItemCount() {
        if (schoolList != null)
            return schoolList.size();
        else
            return 0;
    }

    public interface SchoolSelectedListener {
        void onSelect(School school);
    }
}
