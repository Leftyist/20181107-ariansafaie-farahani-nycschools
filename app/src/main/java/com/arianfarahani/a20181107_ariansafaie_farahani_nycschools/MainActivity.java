package com.arianfarahani.a20181107_ariansafaie_farahani_nycschools;

import android.os.Bundle;
import android.view.MenuItem;

import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.detail.SchoolDetailFragment;
import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.list.SchoolListFragment;
import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model.SatResults;
import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model.School;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initMain();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, SchoolListFragment.newInstance())
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        int backstack = getSupportFragmentManager().getBackStackEntryCount();
        if(backstack > 0)
            initMain();

        super.onBackPressed();
    }

    // I'd normally have a whole separate navigation framework in a larger app
    public void showDetails(School school, SatResults results) {
        setTitle(""); // Set title to blank, felt redundant with title both in toolbar and text

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, SchoolDetailFragment.newInstance(school, results))
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(null)
                .commit();
    }

    public void setTitle(String title) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(title);
    }

    private void initMain() {
        setTitle(getString(R.string.app_name));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }
}
