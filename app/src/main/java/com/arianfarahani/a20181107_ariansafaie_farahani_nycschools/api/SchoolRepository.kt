package com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.api

import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model.SatResults
import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model.School
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object SchoolRepository {

    // In a larger project I'd probably user dagger to inject this
    private val service = Retrofit.Builder()
        .baseUrl("https://data.cityofnewyork.us/")
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
        .create(WebService::class.java)

    @JvmStatic
    fun fetchSchools(completion: (success: Boolean, results: List<School>?) -> Unit) {
        service.fetchSchoolList().enqueue(object : Callback<List<School>> {
            override fun onResponse(
                call: Call<List<School>>, response: Response<List<School>>
            ) {
                if (response.isSuccessful)
                    completion(true, response.body())
                else
                    completion(false, null)
            }

            override fun onFailure(call: Call<List<School>>, t: Throwable) {
                completion(false, null)
            }
        })
    }

    @JvmStatic
    fun fetchSatResults(completion: (success: Boolean, results: List<SatResults>?) -> Unit) {
        service.fetchSatList().enqueue(object : Callback<List<SatResults>> {
            override fun onResponse(
                call: Call<List<SatResults>>, response: Response<List<SatResults>>
            ) {
                if (response.isSuccessful)
                    completion(true, response.body())
                else
                    completion(false, null)
            }

            override fun onFailure(call: Call<List<SatResults>>, t: Throwable) {
                completion(false, null)
            }
        })
    }
}
