package com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.api;

import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model.SatResults;
import com.arianfarahani.a20181107_ariansafaie_farahani_nycschools.model.School;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

interface WebService {

    @GET("resource/97mf-9njv.json")
    Call<List<School>> fetchSchoolList();

    @GET("resource/734v-jeq5.json")
    Call<List<SatResults>> fetchSatList();
}
